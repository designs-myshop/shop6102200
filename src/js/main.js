'use strict';
var $ = require('jquery');

import 'lightbox2';
import '@popperjs/core';
import * as bootstrap from 'bootstrap';
import lazysizes from 'lazysizes';
// import a plugin
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/blur-up/ls.blur-up';

import {toggleActive, doSort} from './components/sort.js';

(function() {

/*
  PRODUCT LIST SORTING
*/

const listItems = document.querySelectorAll('div.sort-list__list');
const gridItems = document.querySelectorAll('div.sort-list__grid');
const items = [...listItems, ...gridItems];
const products = document.querySelectorAll('.myshp-products__product');

items.forEach(item => {
  item.addEventListener('click', function() {
    // set navbar items for grid or list
    toggleActive(gridItems);
    toggleActive(listItems);

    // set list in correct mode
    if (item.classList.contains('active') && item.classList.contains('sort-list__list')) {
      doSort(products,'list');
    } else if (item.classList.contains('active') && item.classList.contains('sort-list__grid')) {
      doSort(products,'grid');
    }
  });
});

/*
  #PRODUCT LIST SORTING#
*/



/*
  ADD CLASS LAZYLOAD TO ALL IMG
*/

lazysizes.cfg.blurupMode = 'auto';

[...document.getElementsByTagName('img')].forEach((image, i) => {
  image.classList.add('lazyload');
});



  // remove the outer tag to support footer at bottom
  $('#myshp-body').children().unwrap();

  // modification on the address page
  // TODO: modification in myshop backoffice
  if ($('.address').length > 0) {
    var inputFields = $('.address__input input[type="text"], .address__input input[type="email"], .address__input input[type="tel"], .address__input textarea, .address__input select, .address dt textarea');
    var select = $('select');

    inputFields.unwrap();
    inputFields.attr('placeholder', 'input');
    $('.address__input input[type="text"], .address__input input[type="email"], .address__input input[type="tel"], .address__input textarea, .address dt textarea').addClass('form-control');
    select.addClass('form-select');

    $('input[name="b_6"], input[name="d_106"]').css('margin-bottom', '-25px');

    // checklabels
    $('.myshp-form input[type="checkbox"]').parent().addClass('form-check form-check-inline');
    $('input[type="checkbox"]').addClass('form-check-input');
    $('.myshp-form input[type="checkbox"]').parent().find('label').addClass('form-check-label');

    $('input[type="radio"]').addClass('form-check-input me-2');
    $('input[type="radio"]').parent().find('label').addClass('form-check-label');

    // remove labels
    $('.form-floating').each(function() {
      $(this).find('input[type="radio"].form-check-input').parent('span').next().remove();
      $(this).find('input[type="checkbox"].form-check-input').parent('span').next().remove();
    });

  }

  // TODO: improve navbar myshop variables
  if (document.querySelector('.navbar-nav')) {
    let navbarItems = document.querySelectorAll(".navbar-nav li.nav-item");

    navbarItems.forEach((navbarItem, index) => {
      let navLink = navbarItem.querySelector('a');
      let navId = navLink.id;

      navLink.setAttribute('class', 'nav-link');

      if (navbarItem.querySelector('ul') != null) {
        let navList = navbarItem.querySelector('ul');

        navbarItem.setAttribute('class', 'dropdown nav-item');
        navLink.setAttribute('data-bs-toggle', 'dropdown');
        navLink.setAttribute('aria-expanded', 'false');
        navLink.setAttribute('class', 'dropdown-toggle nav-link');
        navList.setAttribute('aria-labelledby', navId);
        navList.querySelectorAll('li a').forEach((item, i) => {
          item.setAttribute('class', 'dropdown-item');
        });
      }
    });
  }

})();
