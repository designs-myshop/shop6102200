
# Consumer single page checkout
----
Template (Bootstrap 5) with a single page checkout (two columns).

## Table of contents
---
1. [Prerequisites](#prerequisites)
2. [Fork, copy or download this template](#fork-copy-or-download-this-template)
3. [Complete and run the project](#complete-and-run-the-project)
4. [Template structure](#template-structure)

## 1. Prerequisites
----

##### 1.1. Install node.js
To check if you have Node.js installed, run this command in your terminal:
```
node -v
```
  Or download node.js at the following link: [nodejs.org](https://nodejs.org/en/).



##### 1.2. Install npm
To confirm that you have npm installed you can run this command in your terminal:
```
npm -v
```
  Or run the following command to install npm:
```
npm install npm@latest -g
```
  For more information about npm: [npmjs.com](https://www.npmjs.com/get-npm)



##### 1.3. Install gulp
```
npm install --global gulp-cli
```
If you have any problems, check: [gulpjs.com](https://gulpjs.com/docs/en/getting-started/quick-start).


## 2. Fork, copy or download this template (optional)
----
Fork, copy or download this template (project). This template is useable for every Bitbucket repository that is connected to a shop.



##### 2.1. Template structure

- apps
- catalogs
- _dist_
- _node_modules_
- order-manager
- pages
- _static_
- src
- gulpfile.js
- _package.json_
- _package-lock.json_
- README.md

## 3. Complete and run the project
---
##### 3.1. Install all the modules into the project
If the `package.json` is updated you will always need to run the command `npm install`. Otherwise you cannot compile the files.

```
npm install
```



##### 3.2. Development mode (run)
Every change you make, will be automatically compiled. You can find the compiled files in the `static` and `dist` folder.  
If you want to check your style or simple changes, you can check it locally by running this command.


- It wil open `http://localhost:3000/`
- Continue to `http://localhost:3000/styleguide.html`


```
gulp default
```



##### 3.3. Commit and push
To see the result of all your changes, you will need to commit and push the changes in your preferred GIT interface. So you can check your changes in the LIVE webshop.  

Sometimes you do not see the changes in the LIVE webshop. So you will need to do a __hard refresh__ `cmd + shift + r` or `ctrl + shift + r` in the browser. If this does not work go the backoffice and follow the instructions:

1. Shop settings > optimizations
2. Page cache
3. `Clear cache`

This will reset the imported CSS and JavaScript file.


## 4. Template structure
---

##### 4.1. Styleguide
You can find your styleguide locally: `http://localhost:3000/styleguide.html`. A styleguide is very useful to check every component in the webshop.

##### 4.2. Other pages
All the pages for the webshop is located at `/pages`, `/catalogs` and `order-manager`.

Particular about this template is that the page in `/pages/address/` contains a two-column based template. Which means the left column contains the address fields and the right column contains the product and price summary.

##### 4.3. SASS structure
`/src/` The style files are component based.   

All the components can be found in the `/components` folder. For example the navbar, footer or slider.   

The `/general` folder contains all the general styling for your webshop. For example all the styling for the headings, colors or helpers.  

In the `/themes` folder you can create all your themes. For example a christmas theme, easter theme or just the standard theme. In these files you can overwrite the existing variables to create your own theme.  

The `_main.scss` includes all the files that you have created. If you created a new file, you will have to include it in the `_main.scss`:

```
@import "folder/file";
```

##### 4.3. JavaScript
The JavaScript/jQuery files are located in the `/src/js` folder. Also the JavaScript files are component based. Every created file, can be imported in the `main.js`.
