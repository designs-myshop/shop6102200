let doSort = function(products, sort) {
  products.forEach((product) => {
    if (sort === "list") {
      product.classList.remove( "col-6", "col-md");
      product.classList.add( "col-12");
    } else if (sort === "grid") {
      product.classList.remove( "col-12");
      product.classList.add( "col-6", "col-md");
    }
  });
}

let toggleActive = function(items) {
  items.forEach(item => {
    item.classList.toggle('active');
  });
}

export {
  toggleActive,
  doSort,
};
