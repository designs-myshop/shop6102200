(function() {
  // items
  let items = document.querySelectorAll('.items');
  let itemsToSave = document.querySelectorAll('.save-items');

  // convert rgb to hex
  const rgb2hex = (rgb) => `#${rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/).slice(1).map(n => parseInt(n, 10).toString(16).padStart(2, '0')).join('')}`;

  // menu sections
  let menuSections = document.querySelectorAll('#sidebarMenu .menu-section');
  let menuColorSection = document.getElementById('colorSidebar');
  let menuFontSection = document.getElementById('fontSidebar');

  // color picker
  let menuSelectedItem = document.getElementById('selectedItem');
  let menuColorPicker = document.getElementById('colorPick');
  let menuColorPickerName = document.getElementById('colorName');

  // fonts
  let menuFontSize = document.getElementById('fontSize');
  let menuFontWeightSlider = document.getElementById('fontWeightSlider');
  let menuFontWeightValue = document.getElementById('fontWeightValue');

  // buttons
  let resetBtn = document.getElementById('reset');
  let reloadBtn = document.getElementById('reload');
  let saveBtn = document.getElementById('save');

  // default and new list to save
  let defaultList = [];
  let newList = [];
  let arrIndex = {};

  let selectedItem;

  // activate tooltips
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
  })

  itemsToSave.forEach((item, i) => {
    preset(item);
  });

  items.forEach((item, i) => {
    item.addEventListener('click', () => {
      selectedItem = item;
      select(selectedItem);
    });
  });

  menuColorPicker.addEventListener('input', () => {
    setSelectedColorItem(selectedItem);
  });
  menuFontSize.addEventListener('input', () => {
    setSelectedFontItem(selectedItem);
  });
  menuFontWeightSlider.addEventListener('input', () => {
    setSelectedFontItem(selectedItem);
  });

  resetBtn.addEventListener('click', () => {
    reset();
  });
  saveBtn.addEventListener('click', () => {
    save();
  });




  // Select an item and deselect other items
  // Prepare the menu for the selected item
  function select(itemToSelect) {
    items.forEach((item, i) => {
      if (item.classList.contains('selected')) {
        item.classList.remove('selected');
      }
    });
    itemToSelect.classList.add('selected');
    prepareMenuForItem(itemToSelect);
  }

  // Set the name of the selected item in the menu
  // Set the correct section for the selected item
  // Prepare the values for the input fields
  function prepareMenuForItem(item) {
    menuSelectedItem.innerText = item.getAttribute('data-sass-name');

    if (item.classList.contains('bg-color')) {
      const hex = rgb2hex(window.getComputedStyle(item).backgroundColor);
      setMenuSection('colorSidebar');

      menuColorPickerName.innerText = hex;
      menuColorPicker.value = hex;
    } else if (item.closest('.font-parent')) {
      const fontWeight = window.getComputedStyle(item).fontWeight;
      const fontSize = window.getComputedStyle(item).fontSize;

      setMenuSection('fontSidebar');
      menuFontWeightSlider.value = fontWeight;
      menuFontWeightValue.innerText = fontWeight;
      menuFontSize.value = parseFloat(fontSize);
    }
  }

  // Show preferred section for the selected item
  function setMenuSection(selectedSection) {
    menuSections.forEach((section, i) => {
      // hide all sections
      if (section.classList.contains('d-block')) {
        section.classList.remove('d-block');
        section.classList.add('d-none');
      }
      // select preferred section
      document.getElementById(selectedSection).classList.remove('d-none');
      document.getElementById(selectedSection).classList.add('d-block');
    });
  }

  // Set color items with the correct color
  function setSelectedColorItem(selectedItem) {
    let newColor = colorPick.value;

    menuColorPickerName.innerText = newColor;
    selectedItem.style.setProperty("background-color", newColor, "important");

    // if the color is for the background, set also the background
    if (selectedItem.classList.contains('bg')) {
      document.querySelectorAll('.bg').forEach((item, i) => {
        item.style.setProperty("background-color", newColor, "important");
      });
    }

    selectedItem.setAttribute('data-sass-value', newColor);
  }

  // Set all the font settings to the items
  // Font settings like size, color, weight, family ...
  function setSelectedFontItem(selectedItem) {
    let newSize = menuFontSize.value;
    let newWeight = menuFontWeightSlider.value;
    let otherItems = document.querySelectorAll('main ' + selectedItem.tagName);

    otherItems.forEach((item, i) => {
      if (item.tagName == selectedItem.tagName) {
        item.style.fontSize = newSize + 'px';
        item.style.fontWeight = newWeight;

        if (item.classList.contains('fs')) {
          item.setAttribute('data-sass-value', newSize + 'px');
        } else if (item.classList.contains('fw')) {
          item.setAttribute('data-sass-value', newWeight);
        }
      }
    });
  }

  // Preset all the items with the current values
  // Setting the data-sass-values
  function preset(item) {
    let obj = {};
    if (item.classList.contains('bg-color')) {
      obj[item.getAttribute('data-sass-id')] = window.getComputedStyle(item).backgroundColor;
      item.setAttribute('data-sass-value', window.getComputedStyle(item).backgroundColor);
    } else if (item.classList.contains('fw')) {
      obj[item.getAttribute('data-sass-id')] = window.getComputedStyle(item).fontWeight;
      item.setAttribute('data-sass-value', window.getComputedStyle(item).fontWeight);
    } else if (item.classList.contains('fs')) {
      obj[item.getAttribute('data-sass-id')] = window.getComputedStyle(item).fontSize;
      item.setAttribute('data-sass-value', window.getComputedStyle(item).fontSize);
    }
    defaultList.push(obj);
  }

  // Reset All the items to the default styles
  function reset() {

    if (confirm("Wil je alle wijzigingen resetten?") == true) {
      // reset all the items with a data-sass-id
      itemsToSave.forEach((item, i) => {
        if (item.getAttribute('data-sass-id') == Object.keys(defaultList[i])[0]) {
          item.setAttribute('data-sass-value', defaultList[i][item.getAttribute('data-sass-id')]);
          item.style = '';
        }
      });

      // reset the styling of all items and deselect current item
      items.forEach((item, i) => {
        item.style = '';
        if (item.classList.contains('selected')) {
          item.classList.remove('selected');
        }
      });

      // reset the background
      document.querySelectorAll('.bg').forEach((item, i) => {
        item.style = '';
      });

      // empty the menu section and set different name for selected item
      setMenuSection('itemSection');
      menuSelectedItem.innerText = 'Niets geselecteerd';

      // reset the list
      newList = defaultList;
      console.log(newList);
    }
  }

  function save() {
    itemsToSave.forEach((item, i) => {
      let obj = {};
      obj[item.getAttribute('data-sass-id')] = item.getAttribute('data-sass-value');

      var index = arrIndex[[item.getAttribute('data-sass-id')]];
      if (index === undefined) {
        index = newList.length;
      }
      arrIndex[[item.getAttribute('data-sass-id')]] = index;
      newList[index] = obj;
    });
    console.log(newList);
  }

  function reload() {

  }

})();
